#include "OpenGL.h"
#include "Constants.h"


bool initGL();

void update();

void render();

void runMainLoop(int val);

void handleKeys(unsigned char key, int x, int y);
