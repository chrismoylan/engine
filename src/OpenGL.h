#ifndef OPENGL_H
#define OPENGL_H

#include <GL/freeglut.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <stdio.h>

#endif
