#ifndef WALL_H
#define WALL_H


class Wall : public LevelElement {

public:
  Wall();

  ~Wall();

  void render();

};

#endif
