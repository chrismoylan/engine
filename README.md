# Engine #

An implementation of a 2D game engine from scratch. Inspired partly by the
[LazyFoo Tutorials](http://lazyfoo.net/SDL_tutorials/lesson07/index.php).

## Requirements ##

* opengl (2+)
* freeglut
* cimg

### optional requirements ###

* astyle (2.04+)
* cppcheck
* valgrind
* doxygen

## Project Structure ##
```
engine
|--> build (compiled objects and binaries)
|--> doc (documentation)
|--> lib (external dependencies)
|--> src (main code tree)
|--> test (unit tests)
```